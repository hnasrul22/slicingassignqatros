const {getElementById, body, title} = document

const toogleDark = document.getElementById("toogleDark");
const toogleIcon = document.getElementById("toogleIcon");
const toTop = document.getElementById("toTop");

toogleDark.onclick = () => {
    body.classList.toggle("dark")
    if(body.classList.contains("dark")){
        toogleIcon.src = "/assets/icons/light.svg"
    }else{
        toogleIcon.src = "/assets/icons/dark.svg"
    }
}

toTop.onclick = () => {
    window.scrollTo({
        top : 0,
        behavior : "smooth"
    })
}

window.onblur = () => document.title = 'Please come back...'
window.onfocus = () => document.title = 'KOKOPI..'