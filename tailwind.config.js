/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./**/*.{html,js}"],
  darkMode:"class",
  theme: {
    extend: {},
    colors : {
      white : '#fafafa',
      gray : '#f1f1f1',
      purple : '#673ab8',
      black : '#444444',
      'dark-semi-black' : '#1c1c1c',
      'dark-black' : '#181818',
      'dark-white' : '#ffffff',
      'trans' : '#00000000'
    }
  },
  plugins: [],
}
